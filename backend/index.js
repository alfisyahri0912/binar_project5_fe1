const cookieSession = require("cookie-session")
const express = require("express")
const cors = require("cors");
const passportSetup = require("./passport")
const passport = require("passport")
const authRoute = require("./routes/auth")
const app = express()
const jwt = require("jsonwebtoken")
app.use(express.json())



// OAUTH
app.use(cookieSession(
    {
        name:"session",
        keys:["lama"],
        maxAge: 24 * 60 * 60 * 100,
    }
))

app.use(passport.initialize())
app.use(passport.session())

app.use(cors({
    origin:"http://localhost:3000",
    methods:"GET,POST,PUT,DELETE",
    credentials : true,
}))


// JWT
const users = [
    {
        id : "1",
        username : "john",
        password : "John1234",
        isAdmin : true
    },
    {
        id : "2",
        username : "jane",
        password : "Jane1234",
        isAdmin : false
    },
]

let refreshTokens = []


app.post("/api/refresh", (req,res)=> {
    //take refresh token from user
    const refreshToken = req.body.token

    //send error if no token or invalid
    if(!refreshToken) return res.status(401).json("not auth")
    if(!refreshTokens.includes(refreshToken)){
        return res.status(403).json("Refresh token is not valid")
    }

    jwt.verify(refreshToken, "myRefreshSecretKey", (err,user)=> {
        err && console.log(err)

        refreshTokens = refreshTokens.filter(token=>token !==refreshToken)

        const newAccessToken = generateAccessToken(user)
        const newRefreshToken = generateRefreshToken(user)

        refreshTokens.push(newRefreshToken)

        res.status(200).json({
            accessToken : newAccessToken,
            refreshToken : newRefreshToken
        })
    })

    //if everything is ok, create new akses token, refresh token and send to user
})

const generateAccessToken = (user)=> {
    return jwt.sign({id:user.id, isAdmin:user.isAdmin}, "mySecretKey", {expiresIn : "15m"})
}

const generateRefreshToken = (user)=> {
    return jwt.sign({id:user.id, isAdmin:user.isAdmin}, "myRefreshSecretKey")
}


app.post("/api/login", (req, res) => {
    const { username, password } = req.body;
    const user = users.find((u) => {
      return u.username === username && u.password === password;
    });
    if(user){
        //Generate an access token
        const accessToken = generateAccessToken(user)
        const refreshToken = generateRefreshToken(user)
        refreshTokens.push(refreshToken)

        res.json({
            username:user.username,
            isAdmin:user.isAdmin,
            accessToken ,
            refreshToken
        })
    }else{
        res.status(400).json("Username or password is incorrect")
    }
})



const verify = (req,res,next) => {
    const authHeader = req.headers.authorization
    if(authHeader){
        const token = authHeader.split(" ")[1]

        jwt.verify(token, "mySecretKey", (err,user)=> {
            if(err){
                return res.status(403).json("Token is not valid")
            }

            req.user = user
            next()
        })
    }else{
        res.status(401).json("You are not auth")
    }
}

app.delete("/api/users/:userId", verify, (req,res)=>{
     if(req.user.id === req.params.userId || req.user.isAdmin){
        res.status(200).json("User has been deleted")
     }else{
        res.status(403).json("Not allowed")
     }
})

app.post("/api/logout", verify, (req,res)=>{
    const refreshToken = req.body.token
    refreshTokens = refreshTokens.filter((token) => token !== refreshToken)
    res.status(200).json("You logout success")
})

app.use("/auth", authRoute)

app.listen("5000", ()=> {
    console.log("server is running")
})